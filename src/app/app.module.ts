import { BrowserModule } from '@angular/platform-browser';
// LOCALE_ID nos provee del token para manejar la localización
import { NgModule,LOCALE_ID } from '@angular/core';

import { registerLocaleData } from "@angular/common";

import { AppComponent } from './app.component';
import localeEs from "@angular/common/locales/es";
import localeFr from "@angular/common/locales/fr";
import { CapitalizadoPipe } from './pipes/capitalizado.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { ContraseñaPipe } from './pipes/contraseña.pipe';

// second parameter is optional
// declarando la fincuón con dos parámetros diferentes podremos usar otro idioma
registerLocaleData( localeEs )
registerLocaleData( localeFr )

@NgModule({
  declarations: [
    AppComponent,
    CapitalizadoPipe,
    DomseguroPipe,
    ContraseñaPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
