import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  nombre    : string    = 'Álvaro'
  nombre2   : string    = 'ÁlVaro aGueRré NaVaRreTe'
  ohanna    : string[]  = ['Álvaro', 'Bianca', 'Bastet', 'Miyu']
  PI        : number    = Math.PI
  porcentaje: number    = 0.1234
  salario   : number    = 1234.5
  fecha     : Date      = new Date
  activar   : boolean   = true
  idioma    : string    = 'en'
  videoUrl  : string    = 'https://www.youtube.com/embed/HnmN0N1466E'


  valorPromesa = new Promise<string>( (resolve) => {
    setTimeout( () => {
      resolve( 'llego la data' )
    }, 4500)
  });

  heroe = {
    nombre: 'Logan',
    clave: 'Wolverine',
    edad: 40,
    direccion: {
      calle: 'Primavera',
      numero: 3
    }
  }

}
