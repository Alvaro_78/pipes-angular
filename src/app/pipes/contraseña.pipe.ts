import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContraseñaPipe implements PipeTransform {

  transform(value: string, mostrar: boolean = true): string {
    //  repeat es nuevo en emcs 6 nos permite repetir según sea la longitud del value
    return ( mostrar ) ? '*'.repeat ( value.length ) : value
  }

}
